package ru.tinkoff.news.dagger.component;

import javax.inject.Singleton;

import dagger.Component;
import ru.tinkoff.news.dagger.module.NetworkModule;
import ru.tinkoff.news.dagger.module.SingletonModule;
import ru.tinkoff.news.details.NewsDetailsModule;
import ru.tinkoff.news.list.NewsListModule;

/**
 * @author Sergey Boishtyan
 */
@Singleton
@Component(modules = {NetworkModule.class, SingletonModule.class})
public interface AppComponent {

    NewsListComponent plus(NewsListModule module);

    NewsDetailsComponent plus(NewsDetailsModule module);
}
