package ru.tinkoff.news.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.tinkoff.news.network.NewsService;

/**
 * @author Sergey Boishtyan
 */
@Module
public class NetworkModule {

    @Singleton
    @Provides
    NewsService provideNewsService() {
        return new Retrofit.Builder()
                .baseUrl("https://api.tinkoff.ru/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(NewsService.class);
    }
}
