package ru.tinkoff.news.dagger;

import ru.tinkoff.news.dagger.component.AppComponent;

/**
 * @author Sergey Boishtyan
 */
public class Injector {

    private static Injector instance;

    private final AppComponent appComponent;

    private Injector(AppComponent appComponent) {
        this.appComponent = appComponent;
    }

    public static synchronized void newInstance(AppComponent appComponent) {
        instance = new Injector(appComponent);
    }

    public static Injector getInstance() {
        return instance;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
