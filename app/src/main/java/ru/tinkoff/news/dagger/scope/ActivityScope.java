package ru.tinkoff.news.dagger.scope;

import javax.inject.Scope;

/**
 * @author Sergey Boishtyan
 */
@Scope
public @interface ActivityScope {

}
