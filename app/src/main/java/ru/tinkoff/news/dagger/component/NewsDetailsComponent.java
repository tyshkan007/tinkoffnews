package ru.tinkoff.news.dagger.component;

import dagger.Subcomponent;
import ru.tinkoff.news.details.NewsDetailsModule;
import ru.tinkoff.news.dagger.scope.ActivityScope;
import ru.tinkoff.news.details.NewsDetailsActivity;

/**
 * @author Sergey Boishtyan
 */
@ActivityScope
@Subcomponent(modules = {NewsDetailsModule.class})
public interface NewsDetailsComponent {

    void inject(NewsDetailsActivity activity);
}
