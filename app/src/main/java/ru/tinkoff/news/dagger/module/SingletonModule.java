package ru.tinkoff.news.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.tinkoff.news.utils.HtmlHelper;

/**
 * @author Sergey Boishtyan
 */
@Module
public class SingletonModule {

    @Provides
    @Singleton
    public static HtmlHelper provideHtmlHelper() {
        return new HtmlHelper();
    }
}
