package ru.tinkoff.news.dagger.component;

import dagger.Subcomponent;
import ru.tinkoff.news.dagger.scope.ActivityScope;
import ru.tinkoff.news.list.NewsListActivity;
import ru.tinkoff.news.list.NewsListModule;

/**
 * @author Sergey Boishtyan
 */
@ActivityScope
@Subcomponent(modules = {NewsListModule.class})
public interface NewsListComponent {

    void inject(NewsListActivity activity);
}
