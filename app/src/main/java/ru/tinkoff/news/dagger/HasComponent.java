package ru.tinkoff.news.dagger;

/**
 * @author Sergey Boishtyan
 */
public interface HasComponent<T> {
    T getComponent();
}
