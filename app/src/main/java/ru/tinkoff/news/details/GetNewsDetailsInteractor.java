package ru.tinkoff.news.details;

import ru.tinkoff.news.data.NewsContent;
import ru.tinkoff.news.network.NewsService;
import ru.tinkoff.news.utils.BaseRxInteractor;
import ru.tinkoff.news.utils.OnResult;


class GetNewsDetailsInteractor extends BaseRxInteractor<NewsContent, NewsContent> {

    private final long newsId;

    private final NewsService newsService;

    GetNewsDetailsInteractor(long newsId, NewsService newsService) {
        super();
        this.newsId = newsId;
        this.newsService = newsService;
    }

    void execute(OnResult<NewsContent> callback) {
        executeInternal(newsService.getNewsContent(newsId), callback);
    }
}
