package ru.tinkoff.news.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.tinkoff.news.R;
import ru.tinkoff.news.dagger.HasComponent;
import ru.tinkoff.news.dagger.Injector;
import ru.tinkoff.news.dagger.component.NewsDetailsComponent;
import ru.tinkoff.news.dagger.scope.ActivityScope;
import ru.tinkoff.news.data.NewsContent;
import ru.tinkoff.news.details.NewsDetailsMvp.Presenter;
import ru.tinkoff.news.utils.AnimatorUtils;


public class NewsDetailsActivity extends AppCompatActivity implements NewsDetailsMvp.View, HasComponent<NewsDetailsComponent> {

    public static final String EXTRA_NEWS = "EXTRA_NEWS";

    @BindView(R.id.details_swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.details_error_view)
    View errorView;
    @BindView(R.id.details_web_view)
    WebView webView;

    @Inject
    @ActivityScope
    Presenter presenter;

    private NewsDetailsComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        initActionBar();
        ButterKnife.bind(this);
        getComponent().inject(this);
        presenter.onViewAttached(this);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());
    }

    @Override
    public NewsDetailsComponent getComponent() {
        if (component == null) {
            component = Injector.getInstance().getAppComponent().plus(new NewsDetailsModule(this));
        }
        return component;
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDetached();
        component = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setNewsTitle(CharSequence title) {
        setTitle(title);
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showData(NewsContent entry) {
        errorView.setVisibility(View.GONE);
        webView.loadData(entry.getContent(), "text/html; charset=UTF-8", null);
    }

    @Override
    public void showError() {
        webView.setVisibility(View.GONE);
        AnimatorUtils.animateErrorView(errorView);
    }
}
