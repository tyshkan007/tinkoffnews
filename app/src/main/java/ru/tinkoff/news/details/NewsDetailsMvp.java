package ru.tinkoff.news.details;

import ru.tinkoff.news.data.NewsContent;


@SuppressWarnings("unused")
interface NewsDetailsMvp {

    interface View {
        void setNewsTitle(CharSequence title);
        void showProgress();
        void hideProgress();
        void showData(NewsContent entry);
        void showError();
    }

    interface Presenter {
        void onViewAttached(View view);
        void onRefresh();
        void onViewDetached();
    }
}
