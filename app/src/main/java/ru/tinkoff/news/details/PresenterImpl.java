package ru.tinkoff.news.details;

import ru.tinkoff.news.data.NewsContent;
import ru.tinkoff.news.data.NewsTitle;
import ru.tinkoff.news.utils.HtmlHelper;
import ru.tinkoff.news.utils.OnResult;


class PresenterImpl implements NewsDetailsMvp.Presenter {

    private final NewsTitle newsTitle;

    private final GetNewsDetailsInteractor interactor;

    private NewsDetailsMvp.View view;

    private HtmlHelper htmlHelper;

    public PresenterImpl(NewsTitle newsTitle, GetNewsDetailsInteractor interactor, HtmlHelper htmlHelper) {
        this.newsTitle = newsTitle;
        this.interactor = interactor;
        this.htmlHelper = htmlHelper;
    }

    @Override
    public void onViewAttached(NewsDetailsMvp.View view) {
        this.view = view;
        getDetails();
    }

    @Override
    public void onRefresh() {
        getDetails();
    }

    @Override
    public void onViewDetached() {
        interactor.unsubscribe();
        view = null;
    }

    void getDetails() {
        CharSequence escaped = htmlHelper.escape(newsTitle.getText());
        view.setNewsTitle(escaped);
        view.showProgress();
        interactor.execute(new OnResult<NewsContent>() {
            @Override
            public void onDataReady(NewsContent content) {
                notifySuccess(content);
            }

            @Override
            public void onError() {
                notifyError();
            }
        });
    }

    private void notifyError() {
        if (view != null) {
            view.hideProgress();
            view.showError();
        }
    }

    private void notifySuccess(NewsContent data) {
        if (view != null) {
            view.hideProgress();
            view.showData(data);
        }
    }
}
