package ru.tinkoff.news.details;

import dagger.Module;
import dagger.Provides;
import ru.tinkoff.news.dagger.scope.ActivityScope;
import ru.tinkoff.news.data.NewsTitle;
import ru.tinkoff.news.network.NewsService;
import ru.tinkoff.news.utils.HtmlHelper;

import static ru.tinkoff.news.details.NewsDetailsActivity.EXTRA_NEWS;

/**
 * @author Sergey Boishtyan
 */
@Module
public class NewsDetailsModule {

    private final NewsDetailsActivity activity;

    NewsDetailsModule(NewsDetailsActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    NewsTitle getNewsTitle() {
        return (NewsTitle) activity.getIntent().getParcelableExtra(EXTRA_NEWS);
    }

    @Provides
    @ActivityScope
    NewsDetailsMvp.Presenter providePresenter(NewsTitle newsTitle, GetNewsDetailsInteractor interactor, HtmlHelper htmlHelper) {
        return new PresenterImpl(newsTitle, interactor, htmlHelper);
    }

    @Provides
    @ActivityScope
    GetNewsDetailsInteractor provideGetNewsDetailsInteractor(NewsTitle news, NewsService service) {
        return new GetNewsDetailsInteractor(news.getId(), service);
    }
}

