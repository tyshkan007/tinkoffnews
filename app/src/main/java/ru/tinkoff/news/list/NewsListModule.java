package ru.tinkoff.news.list;

import dagger.Module;
import dagger.Provides;
import ru.tinkoff.news.dagger.scope.ActivityScope;
import ru.tinkoff.news.network.NewsService;
import ru.tinkoff.news.utils.HtmlHelper;

/**
 * @author Sergey Boishtyan
 */
@Module
public class NewsListModule {

    private final NewsListActivity activity;

    NewsListModule(NewsListActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    ListMvp.Presenter providePresenter(GetNewsListInteractor interactor) {
        return new PresenterImpl(interactor);
    }

    @Provides
    @ActivityScope
    GetNewsListInteractor provideGetNewsListInteractor(NewsService service) {
        return new GetNewsListInteractor(service);
    }

    @Provides
    @ActivityScope
    NewsListAdapter provideAdapter(HtmlHelper htmlHelper) {
        return new NewsListAdapter(activity, htmlHelper, activity);
    }
}
