package ru.tinkoff.news.list;

import java.util.List;

import ru.tinkoff.news.data.NewsTitle;


@SuppressWarnings("unused") // marker interface
interface ListMvp {

    interface View {
        void showData(List<NewsTitle> data);
        void showError();
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        void onViewAttached(View view);
        void onRefresh();
        void onViewDetached();
    }
}
