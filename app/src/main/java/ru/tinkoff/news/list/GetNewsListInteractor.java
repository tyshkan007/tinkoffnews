package ru.tinkoff.news.list;

import java.util.Collections;
import java.util.List;

import ru.tinkoff.news.data.NewsTitle;
import ru.tinkoff.news.network.NewsService;
import ru.tinkoff.news.utils.BaseRxInteractor;
import ru.tinkoff.news.utils.OnResult;
import rx.Observable;

class GetNewsListInteractor extends BaseRxInteractor<List<NewsTitle>, List<NewsTitle>> {

    private NewsService newsService;

    GetNewsListInteractor(NewsService newsService) {
        super();
        this.newsService = newsService;
    }

    @Override
    protected Observable<List<NewsTitle>> beforeSubscribe(Observable<List<NewsTitle>> observable) {
        return observable.map(this::sort);
    }

    void execute(OnResult<List<NewsTitle>> onResult) {
        executeInternal(newsService.getNewsFeed(), onResult);
    }

    private List<NewsTitle> sort(List<NewsTitle> data) {
        Collections.sort(data, (left, right) -> {
            long leftValue = left.getTimestamp();
            long rightValue = right.getTimestamp();
            return (leftValue > rightValue ? -1 : (leftValue == rightValue ? 0 : 1));
        });
        return data;
    }

}
