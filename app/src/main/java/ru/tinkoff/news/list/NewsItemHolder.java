package ru.tinkoff.news.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.tinkoff.news.utils.HtmlHelper;
import ru.tinkoff.news.R;
import ru.tinkoff.news.data.NewsTitle;

/**
 * @author Sergey Boishtyan
 */
class NewsItemHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.list_item_tv_title)
    TextView titleTextView;

    private HtmlHelper htmlHelper;

    NewsItemHolder(View view, HtmlHelper htmlHelper) {
        super(view);
        this.htmlHelper = htmlHelper;
        ButterKnife.bind(this, view);
    }

    public void bindItem(NewsTitle newsTitle) {
        titleTextView.setText(htmlHelper.escape(newsTitle.getText()));
    }
}
