package ru.tinkoff.news.list;

import java.util.List;

import ru.tinkoff.news.data.NewsTitle;
import ru.tinkoff.news.utils.OnResult;


public class PresenterImpl implements ListMvp.Presenter {

    private ListMvp.View view;

    private GetNewsListInteractor interactor;

    public PresenterImpl(GetNewsListInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void onViewAttached(ListMvp.View view) {
        this.view = view;
        loadItems();
    }

    @Override
    public void onRefresh() {
        loadItems();
    }

    @Override
    public void onViewDetached() {
        this.view = null;
        interactor.unsubscribe();
    }

    private void loadItems() {
        if (view == null) {
            return;
        }

        view.showProgress();
        interactor.execute(new OnResult<List<NewsTitle>>() {
            @Override
            public void onDataReady(List<NewsTitle> data) {
                notifySuccess(data);
            }

            @Override
            public void onError() {
                notifyError();
            }
        });
    }

    private void notifySuccess(List<NewsTitle> data) {
        if (view == null) {
            return;
        }
        view.hideProgress();
        view.showData(data);
    }

    private void notifyError() {
        if (view == null) {
            return;
        }
        view.hideProgress();
        view.showError();
    }
}
