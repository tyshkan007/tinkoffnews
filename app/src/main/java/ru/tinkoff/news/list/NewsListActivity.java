package ru.tinkoff.news.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.tinkoff.news.R;
import ru.tinkoff.news.dagger.HasComponent;
import ru.tinkoff.news.dagger.Injector;
import ru.tinkoff.news.dagger.component.NewsListComponent;
import ru.tinkoff.news.dagger.scope.ActivityScope;
import ru.tinkoff.news.data.NewsTitle;
import ru.tinkoff.news.details.NewsDetailsActivity;
import ru.tinkoff.news.utils.AnimatorUtils;


public class NewsListActivity extends AppCompatActivity implements ListMvp.View, NewsListAdapter.Callback, HasComponent<NewsListComponent> {

    @BindView(R.id.news_list_swipe_refresh)
    SwipeRefreshLayout swipeRefreshView;
    @BindView(R.id.news_list_error_view)
    View errorView;
    @BindView(R.id.news_list_recycler)
    RecyclerView recyclerView;

    @Inject
    @ActivityScope
    ListMvp.Presenter presenter;

    @Inject
    @ActivityScope
    NewsListAdapter adapter;

    private NewsListComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);
        ButterKnife.bind(this);
        getComponent().inject(this);
        recyclerView.setAdapter(adapter);
        presenter.onViewAttached(this);
        swipeRefreshView.setOnRefreshListener(() -> presenter.onRefresh());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDetached();
        component = null;
    }

    @Override
    public void showData(List<NewsTitle> data) {
        errorView.setVisibility(View.GONE);
        adapter.setData(data);
        AnimatorUtils.animateNewsList(getResources(), recyclerView);
    }

    @Override
    public void showError() {
        recyclerView.setVisibility(View.GONE);
        AnimatorUtils.animateErrorView(errorView);
    }

    @Override
    public void showProgress() {
        swipeRefreshView.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeRefreshView.setRefreshing(false);
    }

    @Override
    public void onItemClicked(NewsTitle item) {
        Intent intent = new Intent(this, NewsDetailsActivity.class);
        intent.putExtra(NewsDetailsActivity.EXTRA_NEWS, item);
        startActivity(intent);
    }

    @Override
    public NewsListComponent getComponent() {
        if (component == null) {
            component = Injector.getInstance().getAppComponent().plus(new NewsListModule(this));
        }
        return component;
    }
}
