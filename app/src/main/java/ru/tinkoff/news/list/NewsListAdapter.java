package ru.tinkoff.news.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.tinkoff.news.R;
import ru.tinkoff.news.data.NewsTitle;
import ru.tinkoff.news.utils.HtmlHelper;


class NewsListAdapter extends RecyclerView.Adapter implements View.OnClickListener {

    interface Callback {
        void onItemClicked(NewsTitle item);
    }

    private List<NewsTitle> data;

    private final HtmlHelper htmlHelper;
    private final LayoutInflater layoutInflater;
    private final Callback callback;

    NewsListAdapter(Context context, HtmlHelper htmlHelper, Callback callback) {
        layoutInflater = LayoutInflater.from(context);
        this.htmlHelper = htmlHelper;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.list_item_news, parent, false);
        v.setOnClickListener(this);
        return new NewsItemHolder(v, htmlHelper);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NewsItemHolder viewHolder = (NewsItemHolder) holder;
        NewsTitle newsTitle = data.get(position);
        viewHolder.bindItem(newsTitle);
        viewHolder.itemView.setTag(newsTitle);
    }

    @Override
    public void onClick(View v) {
        NewsTitle title = (NewsTitle) v.getTag();
        callback.onItemClicked(title);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public void setData(List<NewsTitle> items) {
        this.data = items;
        notifyDataSetChanged();
    }

}
