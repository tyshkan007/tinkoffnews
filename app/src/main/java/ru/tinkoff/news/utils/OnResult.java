package ru.tinkoff.news.utils;

public interface OnResult<T> {
    void onDataReady(T data);
    void onError();
}
