package ru.tinkoff.news.utils;

import ru.tinkoff.news.data.Payload;
import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public abstract class BaseRxInteractor<T, R> {

    private final Scheduler observeOnScheduler;
    private final Scheduler subscribeOnScheduler;
    private Subscription subscription;

    protected BaseRxInteractor(Scheduler observeOnScheduler, Scheduler subscribeOnScheduler) {
        this.observeOnScheduler = observeOnScheduler;
        this.subscribeOnScheduler = subscribeOnScheduler;
    }

    protected BaseRxInteractor() {
        this(AndroidSchedulers.mainThread(), Schedulers.io());
    }

    public void unsubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    protected final void executeInternal(Observable<Payload<T>> observable, OnResult<R> onResult) {
        Observable<T> rawObservable = observable
                .subscribeOn(subscribeOnScheduler)
                .map(Payload::getPayload);
        subscribe(beforeSubscribe(rawObservable), onResult);

    }

    protected Observable<R> beforeSubscribe(Observable<T> observable) {
        return (Observable<R>) observable;
    }

    private void subscribe(Observable<R> observable, OnResult<R> onResult) {
        subscription = observable
                .observeOn(observeOnScheduler)
                .subscribe(onResult::onDataReady, (ignore) -> onResult.onError());
    }
}
