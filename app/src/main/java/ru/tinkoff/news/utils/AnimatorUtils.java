package ru.tinkoff.news.utils;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;

public class AnimatorUtils {

    public static void animateErrorView(View view) {
        float valueFrom = 0f;
        float valueTo = 1f;
        view.setAlpha(valueFrom);
        view.setScaleX(valueFrom);
        view.setScaleY(valueFrom);
        view.setVisibility(View.VISIBLE);
        view.animate()
                .alpha(valueTo)
                .scaleX(valueTo)
                .scaleY(valueTo)
                .setInterpolator(new OvershootInterpolator())
                .start();
    }

    public static void animateNewsList(Resources resources, RecyclerView recyclerView) {
        int displayHeight = resources.getDisplayMetrics().heightPixels;
        recyclerView.setTranslationY(displayHeight);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.animate()
                .translationY(0)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }
}
