package ru.tinkoff.news.utils;

import android.os.Build;
import android.text.Html;

public class HtmlHelper {

    public CharSequence escape(String source) {
        String trim = source.trim();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            //noinspection deprecation
            return Html.fromHtml(trim);
        } else {
            return Html.fromHtml(trim, Html.FROM_HTML_MODE_LEGACY);
        }
    }
}
