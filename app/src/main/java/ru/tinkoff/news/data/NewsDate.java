package ru.tinkoff.news.data;

import android.os.Parcel;
import android.os.Parcelable;

@SuppressWarnings("unused")
class NewsDate implements Parcelable {

    private long milliseconds;

    protected NewsDate(Parcel in) {
        milliseconds = in.readLong();
    }

    long getMilliseconds() {
        return milliseconds;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(milliseconds);
    }

    public static final Creator<NewsDate> CREATOR = new Creator<NewsDate>() {
        @Override
        public NewsDate createFromParcel(Parcel in) {
            return new NewsDate(in);
        }

        @Override
        public NewsDate[] newArray(int size) {
            return new NewsDate[size];
        }
    };
}
