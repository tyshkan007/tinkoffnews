package ru.tinkoff.news.data;

@SuppressWarnings("unused")
public class Payload<T> {

    private String resultCode;

    private T payload;

    public String getResultCode() {
        return resultCode;
    }

    public T getPayload() {
        return payload;
    }
}
