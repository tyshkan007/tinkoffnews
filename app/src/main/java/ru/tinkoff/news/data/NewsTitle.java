package ru.tinkoff.news.data;

import android.os.Parcel;
import android.os.Parcelable;

@SuppressWarnings("unused")
public class NewsTitle implements Parcelable {

    private long id;

    private NewsDate publicationDate;

    private String text;

    protected NewsTitle(Parcel in) {
        id = in.readLong();
        publicationDate = in.readParcelable(NewsDate.class.getClassLoader());
        text = in.readString();
    }

    public long getTimestamp() {
        return publicationDate.getMilliseconds();
    }

    public String getText() {
        return text;
    }

    public long getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeParcelable(publicationDate, i);
        parcel.writeString(text);
    }

    public static final Creator<NewsTitle> CREATOR = new Creator<NewsTitle>() {
        @Override
        public NewsTitle createFromParcel(Parcel in) {
            return new NewsTitle(in);
        }

        @Override
        public NewsTitle[] newArray(int size) {
            return new NewsTitle[size];
        }
    };
}
