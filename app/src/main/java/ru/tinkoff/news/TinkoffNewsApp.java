package ru.tinkoff.news;

import android.app.Application;

import ru.tinkoff.news.dagger.Injector;
import ru.tinkoff.news.dagger.component.DaggerAppComponent;

/**
 * @author Sergey Boishtyan
 */
public class TinkoffNewsApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.newInstance(DaggerAppComponent.create());
    }
}
