package ru.tinkoff.news.network;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.tinkoff.news.data.NewsContent;
import ru.tinkoff.news.data.NewsTitle;
import ru.tinkoff.news.data.Payload;
import rx.Observable;


public interface NewsService {

    @GET("news")
    Observable<Payload<List<NewsTitle>>> getNewsFeed();

    @GET("news_content")
    Observable<Payload<NewsContent>> getNewsContent(@Query("id") long id);

}
