package ru.tinkoff.news.details;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import ru.tinkoff.news.data.NewsTitle;
import ru.tinkoff.news.utils.HtmlHelper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NewsDetailsPresenterImplTest {

    private static final String NEWS_TEXT = "text";

    @Mock NewsTitle newsTitle;

    @Mock
    GetNewsDetailsInteractor getNewsDetailsInteractor;

    @Mock NewsDetailsMvp.View view;

    @Mock HtmlHelper htmlHelper;

    private PresenterImpl presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(newsTitle.getId()).thenReturn(Long.valueOf(1));
        when(newsTitle.getText()).thenReturn(NEWS_TEXT);
        when(htmlHelper.escape(anyString())).thenReturn(NEWS_TEXT);
        presenter = new PresenterImpl(newsTitle, getNewsDetailsInteractor, htmlHelper);
    }


    @Test
    public void testInit() throws Exception {
        presenter.onViewAttached(view);

        verify(view).setNewsTitle(eq(NEWS_TEXT));
        verify(view).showProgress();
        verify(getNewsDetailsInteractor).execute(any());
    }

    @Test
    public void testUnsubscribe() throws Exception {
        presenter.onViewDetached();
        verify(getNewsDetailsInteractor).unsubscribe();
    }
}
