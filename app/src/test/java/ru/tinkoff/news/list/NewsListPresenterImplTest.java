package ru.tinkoff.news.list;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

public class NewsListPresenterImplTest {

    @Mock
    GetNewsListInteractor getNewsListInteractor;

    @Mock ListMvp.View view;

    private PresenterImpl presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        presenter = new PresenterImpl(getNewsListInteractor);
    }

    @Test
    public void testInit() throws Exception {
        presenter.onViewAttached(view);
        verify(view).showProgress();
        verify(getNewsListInteractor).execute(any());
    }

    @Test
    public void testUnsubscribe() throws Exception {
        presenter.onViewAttached(view);
        presenter.onViewDetached();
        verify(getNewsListInteractor).unsubscribe();
    }
}
